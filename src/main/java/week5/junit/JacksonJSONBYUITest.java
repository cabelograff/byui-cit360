package week5.junit;

import org.junit.jupiter.api.Test;
import week3.jacksonjson.JacksonJSONBYUI;
import week3.jacksonjson.model.Student;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class JacksonJSONBYUITest {

    @Test
    public void listOfStudentsIsNotEmpty(){
        JacksonJSONBYUI jacksonJSONBYUI = new JacksonJSONBYUI();
        List<Student> studentList = jacksonJSONBYUI.listOfStudents(5);
        assertFalse(studentList.isEmpty());
    }

    @Test
    public void listOfStudentsSize(){
        JacksonJSONBYUI jacksonJSONBYUI = new JacksonJSONBYUI();
        List<Student> studentList = jacksonJSONBYUI.listOfStudents(5);
        assertNotEquals(5, studentList.size());
    }
}
