package week3.jacksonjson;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.github.javafaker.Faker;
import week3.jacksonjson.model.Address;
import week3.jacksonjson.model.Student;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 */
public class JacksonJSONBYUI {

    /**
     *
     * @param args
     */
    public static void main(String[] args){
        JacksonJSONBYUI jacksonJSONBYUI = new JacksonJSONBYUI();
        jacksonJSONBYUI.init();
    }

    /**
     *
     */
    public void init(){
        List<Student> studentList = listOfStudents(5);
        ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();
        try {
            String studentJson = writer.writeValueAsString(studentList);
            println("#### --- JSON File --- ###");
            println(studentJson);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    public List<Student> listOfStudents(int size){
        println("######### --- Init --- #########");
        List<Student> studentList = new ArrayList();
        Faker faker = new Faker();
        int count = 0;
        while (count <= size){
            Student student = new Student(faker.name().fullName(), faker.internet().emailAddress());
            student.setAddress(new Address(faker.address().streetName(), faker.address().city(), faker.address().state()));
            studentList.add(student);
            count ++;
        }
        printCollection(studentList);
        println("######### --- End List --- #########\n");
        return studentList;
    }


    /**
     *
     * @param collection
     */
    private void printCollection(Collection collection){
        for (Object value : collection) {
            println(value);
        }
    }

    /**
     *
     * @param value
     */
    private void println(Object value){
        System.out.println(value);
    }
}
