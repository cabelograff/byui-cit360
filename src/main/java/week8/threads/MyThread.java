package week8.threads;

import java.util.Date;

public class MyThread extends Thread{

    @Override
    public void run(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(new Date() + " : MyThread running...");
    }
}
