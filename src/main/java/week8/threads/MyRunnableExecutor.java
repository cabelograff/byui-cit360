package week8.threads;

import java.util.Date;

public class MyRunnableExecutor implements Runnable{

    private String name;
    private int sleep;

    public MyRunnableExecutor(String name, int sleep){
        this.name = name;
        this.sleep = sleep;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(new Date() + " : MyRunnableExecutor running... "+ name);
    }
}
