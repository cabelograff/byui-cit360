package week8.threads;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Week08 {

    /**
     *
     * @param args
     */
    public static void main(String[] args){
        Week08 week08 = new Week08();
        week08.init();
    }

    /**
     *
     */
    public void init(){
        System.out.println(new Date() + " : Main class started");
        // simple Thread
        Thread t1 = new Thread(new MyThread());
        t1.start();

        new MyRunnable("MyRunnable 1", 2000);
        new MyRunnable("MyRunnable 2", 7000);
        new MyRunnable("MyRunnable 3", 4000);
        new MyRunnable("MyRunnable 4", 5000);
        new MyRunnable("MyRunnable 5", 1000);

        ExecutorService executor = Executors.newFixedThreadPool(2);
        int cont = 1;
        while (cont < 5){
            executor.submit(new MyRunnableExecutor("MyRunnableUsingExecutor "+cont, 5000));
            cont++;
        }
        System.out.println(new Date() + " : Main class stopped");
    }
}
