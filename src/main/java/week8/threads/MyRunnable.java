package week8.threads;

import java.util.Date;

public class MyRunnable implements Runnable{

    private String name;
    private int sleep;

    public MyRunnable(String name, int sleep){
        this.name = name;
        this.sleep = sleep;
        Thread t = new Thread(this, name);
        t.start();
    }

    @Override
    public void run() {
        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(new Date() + " : MyRunnable running... "+ name);
    }
}
