package week2.collections;

import week2.collections.model.Student;

import java.util.*;

/**
 *
 */
public class CollectionsBYUI {

    /**
     *
     * @param args
     */
    public static void main(String[] args){
        CollectionsBYUI collectionsBYUI = new CollectionsBYUI();
        collectionsBYUI.init();
    }

    /**
     *
     */
    public void init(){
        listExample();
        setExample();
        queueExample();
        mapExample();
        dequeExample();
    }

    /**
     *
     */
    private void listExample(){
        println("######### --- Init List --- #########");
        List<Student> studentList = new ArrayList();
        studentList.add(new Student("Batman", "batman@gmail.com"));
        studentList.add(new Student("Robin", "robin@gmail.com"));
        studentList.add(new Student("Frodo", "frodo@gmail.com"));
        studentList.add(new Student("Sam", "sam@gmail.com"));
        studentList.add(new Student("Gandalf", "gandalf@gmail.com"));
        printCollection(studentList);
        println("######### --- End List --- #########\n");
    }

    /**
     *
     */
    private void setExample(){
        println("######### --- Init Set --- #########");
        Set<Student> setStudents = new LinkedHashSet<>();
        setStudents.add(new Student("Batman", "batman@gmail.com"));
        setStudents.add(new Student("Robin", "robin@gmail.com"));
        setStudents.add(new Student("Frodo", "frodo@gmail.com"));
        setStudents.add(new Student("Sam", "sam@gmail.com"));
        setStudents.add(new Student("Gandalf", "gandalf@gmail.com"));
        printCollection(setStudents);
        println("######### --- End Set --- #########\n");
    }

    /**
     *
     */
    private void queueExample(){
        println("######### --- Init Queue --- #########");
        Queue<Student> queueStudent = new ArrayDeque<>();
        queueStudent.add(new Student("Batman", "batman@gmail.com"));
        queueStudent.add(new Student("Robin", "robin@gmail.com"));
        queueStudent.add(new Student("Frodo", "frodo@gmail.com"));
        queueStudent.add(new Student("Sam", "sam@gmail.com"));
        queueStudent.add(new Student("Gandalf", "gandalf@gmail.com"));
        printCollection(queueStudent);
        println("######### --- End Queue --- #########\n");
    }

    /**
     *
     */
    private void mapExample(){
        println("######### --- Init Map --- #########");
        Map<String, Student> mapStudent = new HashMap<>();
        mapStudent.put("Batman", new Student("Batman", "batman@gmail.com"));
        mapStudent.put("Robin", new Student("Robin", "robin@gmail.com"));
        mapStudent.put("Frodo", new Student("Frodo", "frodo@gmail.com"));
        mapStudent.put("Sam", new Student("Sam", "sam@gmail.com"));
        mapStudent.put("Gandalf", new Student("Gandalf", "gandalf@gmail.com"));
        printCollection(mapStudent.entrySet());
        println("######### --- End Map --- #########\n");
    }

    /**
     *
     */
    private void dequeExample(){
        println("######### --- Init Deque --- #########");
        Deque<Student> dequeStudents = new LinkedList<Student>();
        dequeStudents.add(new Student("Batman", "batman@gmail.com"));
        dequeStudents.add(new Student("Robin", "robin@gmail.com"));
        dequeStudents.add(new Student("Frodo", "frodo@gmail.com"));
        dequeStudents.add(new Student("Sam", "sam@gmail.com"));
        dequeStudents.add(new Student("Gandalf", "gandalf@gmail.com"));
        printCollection(dequeStudents);
        println("######### --- End Deque --- #########\n");
    }

    /**
     *
     * @param collection
     */
    private void printCollection(Collection collection){
        for (Object value : collection) {
            println(value);
        }
    }

    /**
     *
     * @param value
     */
    private void println(Object value){
        System.out.println(value);
    }
}
